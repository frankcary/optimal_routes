var webpack = require('webpack');
/*
//var uglifyJsPlugin = webpack.optimize.UglifyJsPlugin;
var ExtractTextPlugin = require('extract-text-webpack-plugin');
*/
var path = require('path');

module.exports = {
    context: __dirname,
    entry: {
        app: './resources/assets/js/webapp/App.js',
        react_router: 'react-router',
        react_dom: 'react-dom'
    },
    output: {
        path: path.resolve(__dirname, 'public/js/webapp'),
        filename: '[name].js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015']
                }
            },
            { 
                test: /\.css$/,
                loader: "style-loader!css-loader"
            }
        ]
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            name: ['react_dom', 'react_router'] 
        })
    ]
}

