<?php

namespace App\Http\Controllers;

use DB;

class App extends Controller
{
    // 有效期 10 年
    const PERIOD = 10 * 365 * 24 * 60 * 60;
    // uid长度, 6*2为12位
    const UID_STR_LEN = 6;
    // meeting_id长度, 3*2为6位
    const MEETING_ID_STR_LEN = 4;

    /*
     * 高德amap
     *
     * */
    // url公共部分
    const COMMON_URL_AMAP = 'http://restapi.amap.com';
    // 公交接口
    const TRANSIT_PORT_AMAP = '/v3/direction/transit/integrated?';
    // 批量接口
    const BATCH_PORT_AMAP = self::COMMON_URL_AMAP . '/v3/batch?key=';


    // TODO poi详细
    // http://restapi.amap.com/v3/place/detail?key=0074ecb46a997c141338fd419009d66b&id=B0FFFZ7A7D

    /*
     * webapp首页
     * */
    public function index($meeting_id = ''){

        // TODO destination 需要有详细（经纬度、站点名称）
        $destination = '';
        $name = '';
        $rtData = [];

        // 如果有聚会就将路线、聚会人员查出来
        $hasMeeting = !empty($meeting_id);
        if ($hasMeeting) {
            // 找出所有聚会人
            $meeting = DB::table('meetings')
                ->where(['_meeting_id'=>$meeting_id])
                ->first();
            $peopleIds =$meeting['people'];
            $destObj = $meeting['destination'];
            $destination = $destObj['location'];
            $name = $destObj['name'];
            $meetingPeople = DB::table('meeting_people')
                ->whereIn('_meeting_people_id', $peopleIds)
                ->get();

            // 获取路线
            $allRoutes = DB::table('routes_data')
                ->where('meeting_id', '=', $meeting_id)
                ->first()['routes'];

            foreach($meetingPeople as $item) {
                unset($item['ip']);
                $itemData = [
                    'info' => $item
                ];
                if (isset($allRoutes['routes'][$item['location']])) {
                    $itemData['route'] = $allRoutes['routes'][$item['location']];
                }
                array_push($rtData, $itemData);
            }
        }

        // 显示该用户一些信息
        $uid = self::getClientUid();
        $user = [];
        $historyMeeting = [];
        if (!empty($uid)) {
            // 如果存在用户信息需要读区用户信息并返回
            $user = DB::table('users')
                ->where('_uid', '=', $uid)
                ->first();

            // 读取该用户历史参加的聚会
            $myMeetingPeople = DB::table('meeting_people')
                ->where('uid', '=', $uid)
                ->get();
            foreach($myMeetingPeople as $item) {
                unset($item['_id']);
                unset($item['ip']);
                unset($item['_meeting_people_id']);
                unset($item['uid']);
                unset($item['location']);
                array_push($historyMeeting, $item); 
            }
        }

        // 将输入输出到网页，这样方便前端获取和展现
        $data = [
            'meeting_id' => $meeting_id,
            'destination' => $destination,
            'name'        => $name,
            'meeting_people' => json_encode($rtData),
            'user' => json_encode($user),
            'history_meeting' => json_encode($historyMeeting)
        ];
        return view('webapp.app', ['data' => $data]);
    }

    /*
     * 周边列表接口
     * */
    public function api_around_list() {
        // TODO 附近咖啡、美食等
        // http://restapi.amap.com/v3/place/around?key=0074ecb46a997c141338fd419009d66b&location=116.435,39.9424&city=110000&types=050500&offset=25
        $relativePath = '/v3/place/around?city=110000&offset=25';
        $randKey = self::getRandomAmapKey();
        $location = $_GET['location'];
        $types = $_GET['types'];

        $url = self::COMMON_URL_AMAP . $relativePath . 
            '&key=' . $randKey .
            '&location=' . $location.
            '&types=' . $types;

        echo self::_curl_get($url);
    }

    /*
     * 自动登录
     * */
    public function auto_login() {
        $isExistUser = false;
        $uid = null;

        // 是否是小程序
        $isMiniProgram = false;

        // 小程序登录
        if (isset($_GET['isMiniProgram']) && $_GET['isMiniProgram'] == 1) {
            $isMiniProgram = true;
            if (isset($_GET['_uid']) && !empty($_GET['_uid'])) {
                $uid = $_GET['_uid'];
            } 
        // webapp登录
        } else {
            if (isset($_COOKIE['_uid']) && !empty($_COOKIE['_uid'])) {
                $uid = $_COOKIE['_uid'];
            } 
        }

        $rt = [
            'status' => 'ok',
            'data' => []
        ];
        // 查询数据库是否真实存在这个用户
        $user = DB::table('users')->where('_uid', '=', $uid)->first();
        if (!empty($user)) {
            $isExistUser = true;
            $rt['data'] = $user;
        }

        // 不存在用户就生成一个
        if (!$isExistUser) {
            $newUid = bin2hex(random_bytes(self::UID_STR_LEN));
            $newUser = [
                '_uid'        => $newUid,
                'create_time' => date('Y-m-d H:i:s')
            ];
            DB::table('users')->insert($newUser);

            // 非小程序需要设置cookie
            if (!$isMiniProgram) {
                setcookie("_uid", $newUid, time() + self::PERIOD); 
            }

            $rt['data'] = $newUser;
        }
        return json_encode($rt);
    }

    /*
     * 创建一个聚会
     * 初始化聚会参数
     * */
    public function create_meeting() {
        // 聚会记录id
        $newMeetingId = bin2hex(random_bytes(self::MEETING_ID_STR_LEN));
        // 创建聚会记录
        DB::table('meetings')->insert([
            '_meeting_id'   => $newMeetingId,
            'people'        => [],
            'create_time'  => time(),
            'destination'   => [
                'name'     => '',
                'address'  => '',
                'location' => ''
            ]
        ]);
        $rt = [
            'status' => 'ok',
            'data'   => [
                'meeting_id' => $newMeetingId
            ]
        ];
        echo json_encode($rt);
    }



    /*
     * 增加聚会人
     * */
    public function add_people() {
        $uid = self::getClientUid();
        $meetingId = $_GET['meeting_id'];

        // 查看该uid是否在在该meeting下
        // 不在的话就可以增加
        // 在该meeting下就不增加
        $meetingPeopleIds = DB::table('meetings')
            ->where('_meeting_id', '=', $meetingId)
            ->first()['people'];

        // 查出该meeting下所有的uid
        $meetingPeople = DB::table('meeting_people')
            ->whereIn('_meeting_people_id', $meetingPeopleIds)
            ->get();

        $isJoined = false;

        foreach($meetingPeople as $item) {
            if ($uid == $item['uid']) {
                $isJoined = true;
                break; 
            } 
        }

        // 已经加入的需要提示已加入
        if ($isJoined && false) {
            $rt = [
                'status' => 'error',
                'errmsg' => '您已加入该聚会'
            ];
        } else {
            if (isset($_GET['address']) && 
                isset($_GET['location']) &&
                isset($_GET['name'])) {
                // 生成聚会人
                $meetingPeopleId = bin2hex(random_bytes(self::UID_STR_LEN));
                // 创建聚会人
                $meeting_people = [
                    '_meeting_people_id' => $meetingPeopleId,
                    'meeting_id'         => $meetingId,
                    'uid'                => $uid,
                    'ip'                 => $_SERVER['REMOTE_ADDR'],
                    'location'           => $_GET['location'],
                    'address'            => $_GET['address'],
                    'name'               => $_GET['name'],
                    'isInitiator'        => 0,
                    'create_time'        => time()
                ];

                // 设置发起人
                $originalPeople = DB::table('meetings')
                    ->where('_meeting_id', '=', $meetingId)
                    ->get()[0]['people'];
                if (count($originalPeople) == 0) {
                    $meeting_people['isInitiator']  = 1;
                }

                // 记录该聚会人到聚会人表
                DB::table('meeting_people')->insert($meeting_people);

                // 将id更新聚会表
                array_push($originalPeople, $meetingPeopleId);
                DB::table('meetings')
                    ->where('_meeting_id', $meetingId)
                    ->update(['people' => $originalPeople]);

                unset($meeting_people['ip']);

                $rt = [
                    'status' => 'ok',
                    'data'   => [
                        'info'  => $meeting_people
                    ]
                ];
            } else {
                $rt = [
                    'status'  => 'error',
                    'errmsg'  => '没有填写相应地址!'
                ];
            }
        }
        echo json_encode($rt);
    }

    // 获取用户端uid，没有uid返回null
    private function getClientUid() {
        $cuid = null;

        if (isset($_COOKIE['_uid'])) {
            $cuid = $_COOKIE['_uid']; 
        } else if (isset($_GET['_uid'])) {
            $cuid = $_GET['_uid'] ;
        }

        return $cuid;
    }


    /*
     * 计算接口
     * */
    public function calculate() {
        $meeting_id = $_GET['meeting_id'];

        // 每次都进行重新计算
        DB::table('meetings')
            ->where(['_meeting_id'=>$meeting_id])
            ->update(['destination.location' => '']);

        // 先检查数据库是否已经有计算好的地点
        $objDesti = DB::table('meetings')
            ->where(['_meeting_id' => $meeting_id])
            ->first();

        if (empty($objDesti['destination']['location'])) {
            // 获取所有出发地点
            $meetingPeople = self::getAllOrigins($meeting_id);

            if (count($meetingPeople) === 1) {
                $rt = [
                    'status' => 'status',
                    'errmsg' => '只有一个不能参加聚会'
                ]; 
                echo json_encode($rt);
                exit();
            }

            // 计算出附近20个地铁站
            $near20 = self::getNearBy20Sites($meetingPeople);

            // 多线程抓取20个地铁站的路线
            $originalData = self::multiThreadSpider($meetingPeople, $near20);
            
            // 排序得出结果
            $destinationObj  = self::optimal($meeting_id, $originalData);
            $destination  = $destinationObj['destination'];

            $destSite = null;
            foreach($near20 as $oneSite) {
                if ($destination == $oneSite['sl']) {
                    $destSite = $oneSite; 
                }            
            }

            if (!empty($destSite)) {
                // 将结果写入到数据库
                DB::table('meetings')
                    ->where(['_meeting_id'=>$meeting_id])
                    ->update([
                        'destination.location' => $destSite['sl'],
                        'destination.name' => $destSite['n']
                    ]);
            }

            // 将路线写入到数据库
            DB::table('routes_data')->insert([
                'meeting_id' => $meeting_id,
                'routes' => $destinationObj
            ]);
        } else {
            $destination = $objDesti['destination']['location']; 
        }

        // 找出所有聚会人
        $peopleIds = DB::table('meetings')
            ->where(['_meeting_id'=>$meeting_id])
            ->first()['people'];

        $meetingPeople = DB::table('meeting_people')
            ->whereIn('_meeting_people_id', $peopleIds)
            ->get();

        // 整理返回结果 
        $rtData = [];
        foreach($meetingPeople as $item) {
            unset($item['ip']);
            $itemData = [
                'info' => $item,
            ];

            if (isset($destinationObj['routes'][$item['location']])) {
                $itemData['route'] = $destinationObj['routes'][$item['location']];
            }
            array_push($rtData, $itemData);
        }

        $rt = [
            'status'  => 'ok',
            'data'    => [
                'destination' => $destination,
                'meeting_people'      => $rtData
            ] 
        ];
        echo json_encode($rt);
    }

    /*
     * 计算出最优地铁站
     * */
    private function optimal($_meeting_id, $originalData) {
        // 最终存储结构
        $destinationDurations = [];
        // 多个人
        $tempi = 0;
        foreach ($originalData as $item) {

            // 一个人到多个地铁站路线
            $routes_data = json_decode($item);
            foreach($routes_data as $onePeopleToOneSite) {
                // 到一个地铁站的第一条路线
                if (isset($onePeopleToOneSite->body->route)) {
                    $selectIngRoutes = $onePeopleToOneSite->body->route;
                    if (isset($selectIngRoutes->transits)
                        &&
                        is_array($selectIngRoutes->transits)
                        &&
                        count($selectIngRoutes->transits) != 0) {
                        $destination = $selectIngRoutes->destination;
                        $shorestDuration = $selectIngRoutes->transits[0]->duration;
                        // 存储到固定数组中
                        if (!isset($destinationDurations[$destination])) {
                            $destinationDurations[$destination] = [
                                'durations'   => [],
                                'destination' => $destination,
                                'routes'      => []
                            ];
                        }

                        $tempOr = $selectIngRoutes->origin;
                        $destinationDurations[$destination]['routes'][$tempOr] = $selectIngRoutes->transits[0];
                        array_push($destinationDurations[$destination]['durations'], intval($shorestDuration));
                    }
                }
            }
        }

        // sum max mean median std 
        $factorDurations = self::initFactor($destinationDurations);

        // sort
        $rt = self::getOptimalDestination($factorDurations);

        return $rt[0];
    }

    /*
     * 排序算法，找出最优时间
     * */
    private function getOptimalDestination($_factorDurations) {
        // 根据sum过滤出sum小于平均数和中位数的结果
        $filterResult = self::filterOutMaxSum($_factorDurations);

        // 剔除所有参数都大的结果
        $filterResult = self::filterAllFactorMax($filterResult);

        // object转化为数组更方便
        $filterResult = self::objectToArray($filterResult);

        // 各因子的离散离散程度越大就按照该因子排序,循环剔除最后一个结果
        $rt= self::getLastResult($filterResult);

        return $rt;
    }

    /*
     * 使用离散系数递归出最终结果
     * */
    private function getLastResult ($_filterResult) {
        $rtCount = count($_filterResult);

        if ($rtCount == 1) {
            return $_filterResult; 
        }

        // 按照key进行排序，剔除最后一个结果，直到只剩下一个结果
        $tempKey = self::getMaxCvKey($_filterResult);
        //echo $tempKey;
        $maxPos = 0;
        $maxVal = $_filterResult[0][$tempKey];
        for ($j = 1; $j < $rtCount; $j++) {
            if ($_filterResult[$j][$tempKey] > $maxVal) {
                $maxVal = $_filterResult[$j][$tempKey];
                $maxPos = $j;
            } 
        } 

        // 删除 最大值
        array_splice($_filterResult, $maxPos, $maxPos + 1);

        // 递归调用循环处理
        return self::getLastResult($_filterResult);
    }

    /*
     * object to array 将对象转化为数组
     * 'prperty' to array
     * */
    private function objectToArray($obje) {
        $rt = [];
        foreach($obje as $key=>$val) {
            array_push($rt, $val); 
        } 
        return $rt;
    }

    /*
     * 获取最大离散系数的因子key
     * */
    private function getMaxCvKey($_filterResult) {
        $factorKeys = ['mean', 'std', 'max', 'median', 'sum'];
        $keysCount = count($factorKeys);
        $maxCvKey = '';
        $allFactor = [];
    
        foreach($_filterResult as $key => $val) {
            for ($i = 0; $i < $keysCount; $i++) {
                $fkey = $factorKeys[$i];
                if (!isset($allFactor[$fkey])) {
                    $allFactor[$fkey] = [];
                }
                array_push($allFactor[$fkey], $val[$fkey]);
            }
        }

        // 获取所有离散系数 
        $maxDefaultKey = '';
        $maxDefaultCv = 0;
        foreach ($allFactor as $key => $val) {
            $tempCv = self::getCv($val);
            if ($tempCv > $maxDefaultCv) {
                $maxDefaultKey = $key;
                $maxDefaultCv = $tempCv; 
            }
        }
        return $maxDefaultKey;
    }

    /*
     * 获取一个数组的离散程度值
     * std /mean
     * */
    private function getCv($arr) {
        $tempMean = array_sum($arr) / count($arr);
        $tempStd = self::standard_deviation($arr);
        return $tempStd / $tempMean;
    }

    /* 
     * 剔除结果列表中所有因子都大于其中一个结果的因子
     * */
    private function filterAllFactorMax($toFilterAllMax){
        $cloneToFilterAllMax = unserialize(serialize($toFilterAllMax));
        foreach ($toFilterAllMax as $key => $val) {
            foreach($cloneToFilterAllMax as $newKey => $newVal) {
                if( $val['sum'] > $newVal['sum'] &&
                    $val['median'] > $newVal['median'] &&
                    $val['std'] > $newVal['std'] &&
                    $val['mean'] > $newVal['mean'] &&
                    $val['max'] > $newVal['max']
                ) {
                    unset($toFilterAllMax[$key]); 
                } 
            } 
        }
        return $toFilterAllMax;
    }

    /* 
     * 按照sum由小到大排序，先剔除sum很大的
     * 算出所有sum的平均数sumMean和中位数sumMedian
     * 将sum小于sumMean和sumMedian的的destination剔除
     * */
    private function filterOutMaxSum($toFilterMaxData) {
        $rt = [];

        // 计算sumMean and sumMedian
        $sumArr = [];
        foreach ($toFilterMaxData as $key => $val) {
            array_push($sumArr, $val['sum']);
        }

        $sumMedian = self::calculate_median($sumArr);
        $sumMean = array_sum($sumArr) / count($sumArr);

        foreach($toFilterMaxData as $key => $val) {
            if ($val['sum'] < $sumMean && $val['sum'] < $sumMedian) {
                $rt[$key] = $val;
            }
        }
        return $rt;
    } 

    /*
     * 计算单个目的地的 sum max mean median and std 
     * */
    private function initFactor($_destinationDurations) {
        foreach ($_destinationDurations as $key => $val) {
            $durationSum = array_sum($val['durations']);
            $durationsCount = count($val['durations']);

            $_destinationDurations[$key]['sum'] = $durationSum; 
            $_destinationDurations[$key]['max'] = max($val['durations']); 
            $_destinationDurations[$key]['median'] = self::calculate_median($val['durations']); 
            $_destinationDurations[$key]['std'] = self::standard_deviation($val['durations']); 
            $_destinationDurations[$key]['mean'] = $durationSum / $durationsCount; 
        }

        return $_destinationDurations;
    }


    /*
     * 标准差函数
     * */
    private function standard_deviation($aValues, $bSample = false){
        $fMean = array_sum($aValues) / count($aValues);
        $fVariance = 0.0;
        foreach ($aValues as $i){
            $fVariance += pow($i - $fMean, 2);
        }
        $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
        return (float) sqrt($fVariance);
    }

    /*
     * 中位数函数
     * */
    private function calculate_median($arr) {
        $count = count($arr); //total numbers in array
        $middleval = floor(($count-1)/2); // find the middle value, or the lowest middle value
        if($count % 2) { // odd number, middle is the median
            $median = $arr[$middleval];
        } else { // even number, calculate avg of 2 medians
            $low = $arr[$middleval];
            $high = $arr[$middleval+1];
            $median = (($low+$high)/2);
        }
        return $median;
    }


    /*
     * 多线程抓取每个出发点到20个地铁站的数据
     * 每个出发点开启一个线程
     *
     * */
    private function multiThreadSpider($_meetingPeople, $_near20) {
        $mh = curl_multi_init();
        $tempRecord = [];
        for ($i = 0; $i < count($_meetingPeople); $i++) {
            $onePepple = $_meetingPeople[$i];
            $mpid = $onePepple['_meeting_people_id'];
            $tempRecord[$mpid] = [];
            $tempRecord[$mpid]['meeting_id'] = $onePepple['meeting_id'];

            $randKey = self::getRandomAmapKey();
            $batchPort = self::BATCH_PORT_AMAP . $randKey;
            $transitComParams = 'city=北京市&key=' . $randKey . '&';
            $transitPort = self::TRANSIT_PORT_AMAP . $transitComParams;

            // post data
            $sendData = [
                'ops' => [] 
            ];
            for ($j = 0; $j < count($_near20); $j++) {
                $locStr = 'destination=' . $_near20[$j]['sl'] . '&origin=' . $onePepple['location'];
                array_push($sendData['ops'], [
                    'url' => $transitPort . $locStr
                ]);
            }

            $headers = [
                'Content-Type:application/json' 
            ];

            // 请求
            $tempRecord[$mpid]['ch'] = curl_init();
            curl_setopt($tempRecord[$mpid]['ch'], CURLOPT_URL, $batchPort);
            curl_setopt($tempRecord[$mpid]['ch'], CURLOPT_POST, 1);
            curl_setopt($tempRecord[$mpid]['ch'], CURLOPT_POSTFIELDS, json_encode($sendData));
            curl_setopt($tempRecord[$mpid]['ch'], CURLOPT_RETURNTRANSFER, 1);
            curl_multi_add_handle($mh, $tempRecord[$mpid]['ch']);
        }

        // Executando consulta
        $active = null; 
        do {
            $mrc = curl_multi_exec($mh, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active and $mrc == CURLM_OK) {
            if(curl_multi_select($mh) === -1){
                usleep(100);
            }
            do {
                $mrc = curl_multi_exec($mh, $active);
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
        } 

        // Obtendo dados de todas as consultas e retirando da fila
        $rt = [];
        foreach($tempRecord as $key=>$val){
            array_push($rt, curl_multi_getcontent($val['ch']));
        } 


        // 移除 handle
        foreach($tempRecord as $key=>$val) {
            curl_multi_remove_handle($mh, $val['ch']);
        }
        
        // Finalizando
        curl_multi_close($mh);

        return $rt;
    }


    /*
     * 随机获取高德key
     * */
    private function getRandomAmapKey() {
        $amapKeys = [
            '118e2e990ed5a3627e0fbcbdb51dd2f1',
            'a51e0aa9cbd73f995499978ce71af4da',
            '0818c15eeea378932e118f0baea5a09d',
            '0074ecb46a997c141338fd419009d66b' 
        ];

        return $amapKeys[rand(0, 3)];
    }

    /*
     * 计算重点点附近20个站点
     * */
    private function getNearBy20Sites($meetingPeople) {
        $rt20 = [];

        // 获取重心点
        $center = self::getTheCenterOfOrigins($meetingPeople);

        // 获取北京市所有地铁站
        $sites = self::getSubWaySites('China', 'Beijing', 'Beijing');

        // 中心点距离每个站点的距离
        $locationContainer = [];
        $locations = [];
        for ($n = 0; $n < count($sites); $n++) {
            if (!in_array($sites[$n]['sl'], $locations)) {
                $sites[$n]['distance'] = self::haversineGreatCircleDistance($center, $sites[$n]['sl']);
                array_push($locationContainer, $sites[$n]);
                array_push($locations, $sites[$n]['sl']);
            }
        }

        // 排序取出前20个站点
        usort($locationContainer, 'self::sortCTpye');
        $rt20 = array_slice($locationContainer, 0, 20);

        return $rt20;
    }

    /*
     * usort排序
     * */
    private function sortCTpye($itemA, $itemB) {
        if ($itemA['distance'] > $itemB['distance']) {
            return 1; 
        } else {
            return -1;
        }
    }


    /*
     * 计算两个经纬度之间的距离
     * */
    private function haversineGreatCircleDistance($locationFrom, $locationTo) {
        $locationFrom = explode(',', $locationFrom);
        $locationTo = explode(',', $locationTo);

        // convert from degrees to radians
        $latFrom = deg2rad($locationFrom[1]);
        $lonFrom = deg2rad($locationFrom[0]);
        $latTo = deg2rad($locationTo[1]);
        $lonTo = deg2rad($locationTo[0]);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
        cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * 6371000;
    }

    /*
     * 获取所有出发点的重心点
     * */
    private function getTheCenterOfOrigins($arg_meetingPeople) {
        $totalLongitude = 0;
        $totalLatitude  = 0;
        $tempLocation = '';

        $meetingPeopleCount = count($arg_meetingPeople);
        for ($i = 0; $i < $meetingPeopleCount; $i++) {
            $tempLocation = explode(',', $arg_meetingPeople[$i]['location']);
            $totalLongitude += $tempLocation[0];
            $totalLatitude += $tempLocation[1];
        }

        return ($totalLongitude / $meetingPeopleCount).','.($totalLatitude / $meetingPeopleCount);
    }

    /*
     * 从数据库获取所有起始经纬度
     * */
    private function getAllOrigins($meetingId) {
        $meetingParty = DB::table('meetings')->where(['_meeting_id'=>$meetingId]);

        $allMeetingPeopleId = $meetingParty->first()['people'];

        $allMeetingPeople = DB::table('meeting_people')
            ->whereIn('_meeting_people_id', $allMeetingPeopleId)
            ->get();


        return $allMeetingPeople;
    }


    /*
     * 轮询更新接口
     * */
    public function update()
    {
        $destination = '';
        $beginTime = time();
        $meeting_id = $_GET['meeting_id'];
        $tempDestination = $_GET['destination'];
        // TODO destination 需要有详细（经纬度、站点名称）
        $rtData = [];

        // 找出所有聚会人
        $clientPeopleCount = $_GET['people_count'];
        $meeting = DB::table('meetings')
            ->where(['_meeting_id'=>$meeting_id])
            ->first();
        $destObj = $meeting['destination'];
        $destination = $destObj['location'];
        $name = $destObj['name'];
        $peopleIds =$meeting['people'];
        if (empty($tempDestination)) {
            while((count($peopleIds) == $clientPeopleCount  && (time() - $beginTime) < 30)) {
                sleep(2);
                $meeting = DB::table('meetings')
                    ->where(['_meeting_id'=>$meeting_id])
                    ->first();
                $destObj = $meeting['destination'];
                $destination = $destObj['location'];
                $name = $destObj['name'];
                $peopleIds =$meeting['people'];
            }
        }
        $meetingPeople = DB::table('meeting_people')
            ->whereIn('_meeting_people_id', $peopleIds)
            ->get();

        // 获取路线
        $allRoutes = DB::table('routes_data')
            ->where('meeting_id', '=', $meeting_id)
            ->first()['routes'];

        foreach($meetingPeople as $item) {
            unset($item['ip']);
            $itemData = [
                'info' => $item
            ];
            if (isset($allRoutes['routes'][$item['location']])) {
                $itemData['route'] = $allRoutes['routes'][$item['location']];
            }
            array_push($rtData, $itemData);
        }

        $rt = [
            'status' => 'ok',
            'data'   => [
                'meeting_people' => $rtData,
                'destination'    => $destination,
                'name'           => $name
            ]
        ];
        echo json_encode($rt);
    }


    // 获取地铁数据
    private function getSubWaySites($state, $province, $city){
        $stateId = DB::table('states')
            ->where('name', '=', $state)
            ->first()['_state_id'];

        $provinceId = DB::table('provinces')
            ->where(['state_id'=>$stateId, 'name'=>$province])
            ->first()['_province_id'];

        $cityId = DB::table('cities')
            ->where(['province_id'=>$provinceId, 'name'=>$city])
            ->first()['_city_id'];

        $subwayData= DB::table('subway')
            ->where(['city_id'=>$cityId])
            ->first()['data'];

        $rtSites = [];
        for ($j = 0; $j < count($subwayData); $j++) {
            $rtSites = array_merge($rtSites, $subwayData[$j]['st']);
        }

        return $rtSites;
    }

    // 输入建议地址
    public function suggest_address() {
        $keywords = $_GET['keywords']; 
        $key = self::getRandomAmapKey();

        $url = self::COMMON_URL_AMAP . '/v3/assistant/inputtips?key='.$key.'&keywords='.$keywords.'&city=北京市';
        echo self::_curl_get($url);
    }

    private function _curl_get($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    // 导入地铁数据
    public function subway() {
        exit();
        // 插入国家 
        $state_id = bin2hex(random_bytes(self::UID_STR_LEN));
        DB::table('states')->insert([
            '_state_id' => $state_id,
            'name'      => 'China'
        ]);


        // 插入省份
        $province_id = bin2hex(random_bytes(self::UID_STR_LEN));
        DB::table('provinces')->insert([
            'state_id'     => $state_id,
            '_province_id' => $province_id,
            'name'         => 'Beijing'
        ]);

        // 插入城市
        $city_id = bin2hex(random_bytes(self::UID_STR_LEN));
        DB::table('cities')->insert([
            'province_id' => $province_id,
            '_city_id'    => $city_id,
            'name'        => 'Beijing'
        ]);

        // 插入地铁数据
        $subway_id = bin2hex(random_bytes(self::UID_STR_LEN));


        $str = file_get_contents('/home/ysh/dev/optimal_routes/temp/BeiJing.data');
        $json = json_decode($str);
        $subwayObj = [
            'city_id'     => $city_id,
            '_subway_id'  => $subway_id,
            'data'        => $json->l
        ];

        DB::table('subway')->insert($subwayObj);

        echo 'ok';
    }

    /**
     * CORS Test
     * */
    public function cors() {
      $rt = [
          'status'     => '0000',
          'data'        => uniqid(),
      ];
      //return json_encode($rt);
      return response($rt, 200)
            ->header('Content-Type', 'application/json')
            ->header('Access-Control-Allow-Origin', 'http://y.dev.youshaohua.com')
            ->header('What-Is-For', 'For CORS');
    }
}
