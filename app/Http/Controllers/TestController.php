<?php

namespace App\Http\Controllers;

use DB;
use App\Mon;

class TestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $data = Mon::test();
        echo gettype($data);
    }

}
