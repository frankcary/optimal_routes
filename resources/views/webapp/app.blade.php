<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <title>派ti</title>
    </head>
    <body>
    <div
    id="app" 
    data-meetingid='<?php echo $data['meeting_id'];?>' 
    data-destination='<?php echo $data['destination'];?>'
    data-name='<?php echo $data['name'];?>'
    data-meeting_people='<?php echo $data['meeting_people'];?>'
    data-user='<?php echo $data['user']?>'
    data-history_meeting='<?php echo $data['history_meeting']?>'
    ></div>
    <script type="text/javascript" src="https://webapi.amap.com/maps?v=1.3&key=7c04cebb6bc0721d9e9d8cc5ffebfd20"></script>
    <script src="/js/webapp/react_router.js"></script>
    <script src="/js/webapp/react_dom.js"></script>
    <script src="/js/webapp/app.js?v=20170421005"></script>
    </body>
</html>
