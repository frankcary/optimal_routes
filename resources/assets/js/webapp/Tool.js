class Tool{
    constructor(...args) {
    }
    // 拷贝对象
    clone(_self) {
        var newSelf = {};
        for (var pro in _self) {
            newSelf[pro] = _self[pro];
        }
        return newSelf;
    }
    // 时间戳格式化
    formatDate(timestamp) {
        var dateStr = '';
        if (timestamp) {
            var dateObj = new Date(timestamp * 1000);  
            dateStr += dateObj.getFullYear() + '-';
            dateStr += (dateObj.getMonth() + 1) + '-';
            dateStr += dateObj.getDate() + ' ';
            dateStr += dateObj.getHours() + ':';
            dateStr += dateObj.getMinutes();
        }
        return dateStr;
    }
}

var tool = new Tool();
export default tool;
