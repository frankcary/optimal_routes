class Http {
    request (url, sendData, callback, syncType) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                var data = this.responseText;
                callback && callback(JSON.parse(data));
            }
        };
        if (sendData) {
            url += '?';
            for (var item in sendData) {
                url += item + '=' + sendData[item] + '&';
            }
        }
        if (syncType && syncType == 'sync') {
            xhttp.open("GET", url, false);
        } else {
            xhttp.open("GET", url, true);
        }
        xhttp.send();
    }

}

export default new Http();
