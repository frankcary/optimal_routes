import React from 'react';
import ReactDOM from 'react-dom';
import Copy from './Copyright';

class Error extends React.Component {
    constructor(...args) {
        super(...args);
    }

    render () {
        return (
            <div className="page p-error">
                <div className="hd"></div>
                <div className="bd">
                    <h3>error，请联系管理员</h3>         
                </div>
                <div className="ft">
                    <Copy />
                </div>
            </div>
        ); 
    }
}

export default Error;
