import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router';
import Map from './map';
import Http from './http';
import Copy from './Copyright';
import Tool from './Tool';

class HistoryMeetingItems extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        var histories = [];
        this.props.history_meeting.map(function(item){
            histories.push(
                <tr key={item.meeting_id} className="item">
                    <td>
                        {item.meeting_id}
                    </td>
                    <td>
                        {Tool.formatDate(item.create_time)}
                    </td>
                    <td>
                        <a href={'/'+item.meeting_id+'/calcute'}>查看</a>
                    </td>
                </tr>
            );
        });
        return (
            <tbody>
                {histories} 
            </tbody>
        );
    }
}

class HistoryMeeting extends React.Component {
    constructor(...args) {
        super(...args);
        var dataset = document.getElementById('app').dataset;
        this.state = {
            history_meeting: JSON.parse(dataset.history_meeting)
        };
    }
    render() {
        if (this.state.history_meeting.length === 0) {
            return false; 
        }
        return (
            <div className="history-meeting">
                <h6>历史派ti</h6>
                <table>
                    <thead>
                        <tr><th>聚会id</th><th>创建时间</th><th>查看</th></tr>
                    </thead>
                    <HistoryMeetingItems history_meeting={this.state.history_meeting} />
                </table>
            </div>
        );
    }
}


class Home extends React.Component {
    constructor(...args) {
        super(...args);
        this.createMeeting = this.createMeeting.bind(this);
    }
    createMeeting() {
        if (!this.props.params.meeting_id) {
            Http.request('/create_meeting', {}, function(rt){
                if (rt.status == 'ok') {
                    browserHistory.push('/' + rt.data.meeting_id + '/address');    
                }
            });
        } 
    }

    render(){
        return (
            <div className="page p-home">
                <div className="hd"></div>
                <div className="bd">
                    <div className="indro">
                        派ti是确定最优聚会地点的服务。通过派ti能在最短时间确定最优聚会地点和参与聚会，从而更多的享受聚会的快乐。
                    </div>
                    <div className="start">
                        <button
                        type="button"
                        onClick={this.createMeeting}>
                            发起聚会
                        </button>
                        <div className="case">
                          <a href="http://partyti.youshaohua.com/87155043/calcute">View a Case</a>
                        </div>
                    </div>
                    <HistoryMeeting />

                </div>
                <div className="ft">
                    <Copy />
                </div>
            </div>
        );
    }
}

export default Home;
