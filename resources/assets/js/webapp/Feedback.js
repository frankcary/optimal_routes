import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';

class Feedback extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        return (
            <div className="page p-feedback">
                <div className="hd"></div>
                <div className="bd">
                    <h3>关于派ti</h3>
                    <p>
                        派ti是确定最优聚会地点的服务。通过派ti能在最短时间确定最优聚会地点和参与聚会，从而更多的享受聚会的快乐。
                    </p>
                    <h3>关于版权</h3>
                    <p>本服务创意点已通过律师事务所向国家提交发明专利保护；请尊重版权。</p>
                    <h3>关于建议</h3>
                    <p className="intro">
                        请将反馈建议发到邮箱shaohua.you@qq.com。
                        <br />
                        多谢你的支持。
                    </p>
                </div>
            </div>
        );
    }
}
export default Feedback;
