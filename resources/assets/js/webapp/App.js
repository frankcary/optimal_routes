import '../../css/webapp/app.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';
// my module
import Home from './Home';
import Address from './Address';
import Calcute from './Calcute';
import Http from './http';
import Feedback from './Feedback';

class App extends React.Component {
    constructor(...args) {
        super(...args);
    }
    componentWillMount() {
        if(top.location!==self.location){
            top.location.href=self.location.href;
        }
        Http.request('/auto_login', {}, function(res) {
            var appDom = document.getElementById('app');
            appDom.setAttribute('data-user', JSON.stringify(res.data));
        });
    }
    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={Home}></Route>

                <Route 
                path="/:meeting_id/address" 
                component={Address}
                ></Route>

                <Route 
                path="/:meeting_id/calcute" 
                component={Calcute}
                ></Route>

                <Route path="/about" component={Feedback}></Route>

                <Route path="/:meeting_id" component={Home}></Route>
            </Router>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('app'));
