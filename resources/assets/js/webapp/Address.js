import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Link, browserHistory } from 'react-router';
import Http from './http';
import Copy from './Copyright';
import Tool from './Tool';


class Li extends React.Component{
    constructor(...args) {
        super(...args);
    }

    render() {
        var str = null;
        // 过滤掉无用的数据
        if (!Array.isArray(this.props.data.location)) {
            str = <li 
                key={this.props.data.id} 
                onClick={this.props.selected} 
                data-address={this.props.data.address}
                data-name={this.props.data.name}
                data-location={this.props.data.location}
                >
                    <h5>{this.props.data.name}</h5>
                    <span>{this.props.data.address}</span>
                </li>
        }
        return str;
    }
}

class Tips extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        return (
            <ul className="sugg-address">
                {this.props.tips.map(function(item, i){
                    return <Li 
                            data={item} 
                            key={item.id + item.location} 
                            selected={this.props.selected} />
                }, this)}
            </ul>
        );
    }
}

class Address extends React.Component {
    constructor(...args) {
        super(...args);
        this.state = {
            tips: [], // 建议列表
            address: '', // 详细地址
            name: '', // 地址名称
            keywords: '', // 关键字
            location: '', // 经纬度
            info: null // 增加后的信息
        };
        this.add = this.add.bind(this);
        this.showSuggest = this.showSuggest.bind(this);
        this.selected = this.selected.bind(this);
    }
    vali(obj) {
        var isPassed = true; 
        for (var pro in obj) {
            if (!obj[pro]) {
                isPassed = false; 
            } 
        }
        return isPassed;
    }
    add(e) {
        var that = this,
            sendData = Tool.clone(that.addressText.dataset);
        // 判断非空
        if (!that.vali(sendData)) {
            alert('地址不能为空'); 
            e.preventDefault();
            return false;
        }
        sendData['meeting_id'] = that.props.params.meeting_id;
        Http.request('/add_people', sendData, function(rt){
            if (rt.status == 'ok') {
                browserHistory.push('/' + that.props.params.meeting_id + '/calcute');    
            }
        });
    }
    showSuggest(e) {
        var keywords = e.target.value,
            that = this;

        this.setState({
            address: '',
            name: '',
            location: '',
            keywords: keywords
        });

        Http.request('/suggest_address', {
            keywords: keywords
        }, function(rt){
            that.setState({tips: rt.tips});
        });
    }
    selected(e) {
        var temp = e.currentTarget.dataset;
        this.setState({
            address: temp.address,
            name: temp.name,
            keywords: temp.name,
            location: temp.location,
            tips: []
        });
    }
    render(){
        return (
            <div className="page p-address">
                <div className="hd"></div>
                <div className="bd">
                    <form>
                        <input
                        type="text"
                        placeholder="你所在小区/楼栋/街道"
                        value={this.state.keywords}
                        data-location={this.state.location}
                        data-name={this.state.name}
                        data-address={this.state.address}
                        onChange={this.showSuggest}
                        ref={(input)=>{this.addressText = input;}}
                        />
                        <button
                        type="button"
                        onClick={this.add}
                        className=""
                        >确定</button>
                    </form>
                    <Tips tips={this.state.tips} selected={this.selected}/>
                </div>
                <div className="ft">
                    <Copy />
                </div>
            </div>
        );
    }
}

export default Address;
