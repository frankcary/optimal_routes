import React from 'react';
import ReactDOM from 'react-dom';
import { Link, browserHistory } from 'react-router';
import Http from './http';
import Copy from './Copyright';

class Map extends React.Component {
    constructor(...args) {
        super(...args);
        this.setMarker = this.setMarker.bind(this);
        this.state = {
            markers: [] 
        };
    }
    componentDidMount() {
        var map = new AMap.Map('map-container',{
            zoom: 10,
            center: [116.39,39.9],
            rotateEnable: true,
            mapStyle: 'light'
        }); 

        AMap.plugin(['AMap.ToolBar'], function(){
            map.addControl(new AMap.ToolBar());
        });
        this.setState({map: map});
    }
    componentDidUpdate() {
        this.setMarker();
    }
    getPolyline(route) {
        var polyline = [];
        for (var i = 0, ilen = route.segments.length; i < ilen; i++) {
            if (route.segments[i].bus && route.segments[i].bus.buslines) {
                var buslines = route.segments[i].bus.buslines;
                for (var j = 0, jlen = buslines.length; j < jlen; j++) {
                    var oneline = buslines[j].polyline.split(';');
                    for (var m = 0, mlen = oneline.length; m < mlen; m++) {
                        polyline.push(oneline[m].split(','));
                    }
                }
            }
        }
        return  polyline;
    } 
    setMarker() {
        // 清空原来的marker
        var tempMap = this.state.map;


        for (var i = 0, ilen = this.props.markers.length; i < ilen; i++) {
            var item = this.props.markers[i];

            var iconStr = '<div class="amap-marker-content">';

            if (item.route) {
                iconStr += '<span class="duration">' +  (Math.floor(item.route.duration/60))+'分</span>';
            }

            iconStr += '<span class="icon"></span></div>';

            // 标记
            new AMap.Marker({
                content: iconStr, 
                position: item.info.location.split(','),
                map: tempMap 
            }); 

            var strokeColor = '000',
                zIndex = 50;
            if (item.info.uid == this.props.user._uid) {
                strokeColor = 'F00';
                zIndex = 100;
            }

            if (item.route) {
                var lineArr = this.getPolyline(item.route);
                var polyline = new AMap.Polyline({
                    path: lineArr,          //设置线覆盖物路径
                    strokeColor: '#' + strokeColor, //线颜色
                    strokeOpacity: 1,       //线透明度
                    strokeWeight: 5,        //线宽
                    strokeStyle: "solid",   //线样式
                    zIndex: zIndex,
                    strokeDasharray: [10, 5] //补充线样式
                });
                polyline.setMap(tempMap);
            }
        }

        // 设置destination
        if (this.props.destination) {
            iconStr = '<div class="amap-marker-content des"><span class="icon"></span></div>';
            new AMap.Marker({
                content: iconStr, 
                position: this.props.destination.split(','),
                map: tempMap 
            }); 
        }
    
        tempMap.setFitView();
    }
    render() {
        return (
            <div 
            id="map-container" 
            style={{height: this.props.maxHeight+ 'px'}}
            className="map-container"></div>
        );
    }
}

class Shopitem extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        return (
            <li className="item">
                <a href={'http://m.amap.com/detail/index/poiid='+this.props.detail.id}>
                    <h6>{this.props.detail.name}</h6>
                    <p>地址: {this.props.detail.address}</p>
                </a>
            </li>
        ); 
    }
}
class Nearby extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        console.log(this.props.nearby);
        var pois = this.props.nearby,
            list = [];
        if (!pois) {
            return false; 
        }
        for (var i = 0, ilen = pois.length; i < ilen; i++) {
            list.push(<Shopitem  detail={pois[i]}/>)
        }
        return (
            <ul className="body-cont">
                {list}
            </ul>
        ); 
    }
}

var isFirstUpdate = true;
class Calcute extends React.Component {
    constructor(...args) {
        super(...args);
        var dataset = document.getElementById('app').dataset,
            markers = JSON.parse(dataset.meeting_people);


        this.state = {
            markers: markers,
            destination: dataset.destination,
            user: JSON.parse(dataset.user),
            name: dataset.name
        };
        this.calcute = this.calcute.bind(this);
        this.updateNearby = this.updateNearby.bind(this);
        this.addPeople= this.addPeople.bind(this);
    }
    componentWillMount(){
        if (isFirstUpdate) {
            var people_count = this.state.markers.length;
            this.updateMarkers(this.props.params.meeting_id,
                    people_count,
                    this.state.destination);
        }
    }
    updateMarkers(meetingId, people_count, destination) {
        var that = this,
            sendData = {
            meeting_id: meetingId,
            people_count: people_count,
            destination: destination,
            nearby_data: null
        };

        if (!that.state.destination) {
            Http.request('/update', sendData, function(res) {
                if (res.status == 'ok') {
                    console.log(res);
                    that.setState({
                        markers: res.data.meeting_people,
                        destination: res.data.destination,
                        name: res.data.name
                    }); 
                    if (!res.data.destination) {
                        that.updateMarkers(meetingId,
                            res.data.meeting_people.length,
                            res.data.destination);
                    }
                }
            });        
        }
    }
    addPeople() {
        browserHistory.push('/' + this.props.params.meeting_id + '/address');    
    }
    calcute() {
        if (confirm('点击“开始计算”后将不能再添加人。')) {
            var sendData = {
                meeting_id: this.props.params.meeting_id 
            },
            that = this;
            
            Http.request('/calculate', sendData, function(res) {
                if (res.status == 'ok') {
                    that.setState({markers: res.data.meeting_people, destination: res.data.destination}); 
                } else {
                    alert(res.errmsg); 
                }
            });        
        }
    }
    updateNearby(e) {
        var currentTypes = e.currentTarget,
            that = this,
            sendData = {
                types: currentTypes.dataset.types ,
                'location': that.state.destination
            };

        Http.request('/api_around_list', sendData, function(res) {
            if (res.status == 1) {
                that.setState({nearby_data: res.pois}); 
            } else {
                alert(res.errmsg); 
            }
        });        

    }
    render(){
        // map max height
        var maxWidth = document.documentElement.clientWidth,
            height = document.documentElement.clientHeight;

        if (maxWidth > height) {
            maxWidth = height - 50; 
        }
        var user = this.state.user,
            isMeetingPeople = false,
            isInitiator =  false;
        this.state.markers.map(function(item){
            if (item.info.uid == user._uid) {
                isMeetingPeople = true;
                if (item.info.isInitiator == 1) {
                    isInitiator = true; 
                }
            }
        });

        var isShowAddPeopleBtn = false;
        if (!isMeetingPeople || isInitiator) {
            isShowAddPeopleBtn = true; 
        }

        var nearbyUrl = 'http://m.amap.com/search/view/keywords=';
        var typeKeywords = [
                {
                    name: '美食',
                    types: '050100|050101|050102|050103|050104|050105|050106|050107|050108|050109|050110|050111|050112|050113|050114|050115|050116|050117|050118|050119|050120|050121|050122|050123|050200|050201|050202|050203|050204|050205|050206|050207|050208|050209|050210|050211|050212|050213|050214|050215|050216|050217|050300|050301|050302|050303|050304|050305|050306|050307|050308|050309|050310|050311|050400'
                },
                {
                    name: '咖啡厅',
                    types: '050500'
                },
                {
                    name: '购物中心',
                    types: '060100'
                },
                {
                    name: '体育场所',
                    types: '080000'
                },
                {
                    name: '景点',
                    types: '110000'
                }
            ];
        var uls = [],
            tabClass = '';
        uls.push(<li><a>附近</a></li>);
        for (var i = 0, ilen = typeKeywords.length; i < ilen; i++) {
            tabClass = (i == 0) ? '': '';
            uls.push(
                <li key={typeKeywords[i].types} className={tabClass}>
                    <a onClick={this.updateNearby} data-types={typeKeywords[i].types}>
                        {typeKeywords[i].name}
                    </a>
                </li>
            );  
        }
        return (
            <div className="page p-calcute">
                <div className="hd"></div>
                <div className="bd">
                    <Map maxHeight={maxWidth} user={this.state.user}  markers={this.state.markers} destination={this.state.destination} />
                    {!this.state.destination ?
                    <div className="btns" style={{marginTop: maxWidth}}>
                        {isShowAddPeopleBtn ? <button onClick={this.addPeople}>参加聚会</button> : ''}
                        {isInitiator ? <button onClick={this.calcute}>开始计算</button>:''}
                    </div>
                    : ''}
                    {this.state.destination ?
                        <div className="rt" style={{marginTop: maxWidth}}>
                            <div className="head">
                                <h3>
                                    终点：{this.state.name}
                                    <Link to="/" className="go-home">
                                        首页
                                    </Link>
                                </h3>
                                <ul>
                                    {uls}
                                </ul>
                            </div>
                            <div className="body">
                                <Nearby nearby={this.state.nearby_data} destination={this.state.destination} />
                            </div>
                        </div>
                    : ''}

                </div>
                <div className="ft">
                </div>
            </div>
        );
    }
}

export default Calcute;
