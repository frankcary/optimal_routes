import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router';

class Copy extends React.Component {
    constructor(...args) {
        super(...args);
    }
    render() {
        return (
            <div className="copy-right">
                <div className="cont">
                &copy; 2017&nbsp;-&nbsp;派ti
                <Link className="feedback" to="/about">关于派ti</Link>
                </div>
            </div>
        );
    }
}
export default Copy;
