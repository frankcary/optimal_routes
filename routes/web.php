<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// webapp 首页

// cros test
Route::get('/api/cors', 'App@cors');

// 地址设置
Route::get('/auto_login', 'App@auto_login');

// 创建一个聚会
Route::get('/create_meeting', 'App@create_meeting');

Route::get('/add_people', 'App@add_people');

// 轮询更新接口
Route::get('/update', 'App@update');

// 计算接口
Route::get('/calculate', 'App@calculate');

// 地址suggest
Route::get('/suggest_address', 'App@suggest_address');

// 地址周边
Route::get('/api_around_list', 'App@api_around_list');

// 地铁数据
Route::get('/subway', 'App@subway');

// about
Route::get('/about', 'App@index');
// webapp首页
Route::get('/{meeting_id?}', 'App@index');
// webapp 地址页面
Route::get('/{meeting_id?}/address', 'App@index');
// webapp计算页面
Route::get('/{meeting_id?}/calcute', 'App@index');

