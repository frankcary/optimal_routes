## optimal_routes
optimal_routes is my personal project that save time when Multi-person gathering.

## Online url (support BeiJing)
http://partyti.youshaohua.com/


### 自动登录接口
/auto_login

### 创建聚会
/create_meeting

### 增加聚会人
/add_people

@param meeting_id 聚会id

@param address    出发地址

@param location   出发经纬度